type person = {
    name: string;
    phone: string;
    address: string;
    email: string;
}

let people: person[] = [];
let colorSwitch = 0;

let personTable: HTMLTableElement = document.createElement("table");

people.push({ name: "tom", phone: "5555555555", address: "23 bakers", email: "tom@gmail.com" });

const first = (input: any) =>
    input[0];

const rest = (input: any) =>
    input.slice(1);

const cons = (list1: any, list2: any) =>
    list1.concat(list2);

const createHeader = () => {
    let header: HTMLHeadElement = document.createElement("thead");
    let headerRow: HTMLTableRowElement = document.createElement("tr");
    headerRow.style.backgroundColor = "lightgrey";
    for (let field in people[0]) {
        let title = cons(first(field).toUpperCase(), rest(field));
        // Create the data
        let headerValue: Text = document.createTextNode(title);
        // create the cells for to hold the data
        let tableHeader: HTMLTableHeaderCellElement = document.createElement("th"); // th
        // button
        let button = document.createElement("button");
        button.id = `btnBy${cons(first(field).toUpperCase(), rest(field))}`;
        button.textContent = cons(first(field).toUpperCase(), rest(field));
        button.style.borderStyle="none";
        button.style.backgroundColor="lightgrey";
        button.style.fontSize="16px";
        button.style.fontStyle="bold";
        button.style.textAlign="left";
        button.style.paddingLeft="0px";
        // insert the data into the cell
        //tableHeader.appendChild(headerValue);
        tableHeader.appendChild(button);
        // add the cell to the row
        headerRow.appendChild(tableHeader);
    }
    header.appendChild(headerRow);
    // Add the row to the table
    personTable?.append(header);
}

const fillTable = () => {
    colorSwitch = 0;
    personTable.innerHTML = "";

    createHeader();

    let tableBody: HTMLTableSectionElement = document.createElement("tbody");
    for (let person of people) {

        let row: HTMLTableRowElement = document.createElement("tr");

        if (colorSwitch % 2 == 0) {
            row.style.backgroundColor = "gainsboro";
        }

        colorSwitch += 1;
        // Create the data for the cells.
        let rowName = document.createTextNode(person.name);
        let rowPhone = document.createTextNode(person.phone);
        let rowAddress = document.createTextNode(person.address);
        let rowEmail = document.createTextNode(person.email);
        // Create the cells.
        let tableRowName = document.createElement("td");
        let tableRowPhone = document.createElement("td");
        let tableRowAddress = document.createElement("td");
        let tableRowEmail = document.createElement("td");
        // put the data into the cells.
        tableRowName.appendChild(rowName);
        tableRowPhone.appendChild(rowPhone);
        tableRowAddress.appendChild(rowAddress);
        tableRowEmail.appendChild(rowEmail);
        // add the cells to the row.
        row.appendChild(tableRowName);
        row.appendChild(tableRowPhone);
        row.appendChild(tableRowAddress);
        row.appendChild(tableRowEmail);
        //add row to table
        tableBody.appendChild(row);
    }

    personTable.appendChild(tableBody);
    document.getElementById("btnAddPerson")?.addEventListener("click", addPerson);
    document.getElementById("btnByName")?.addEventListener("click", sortByName);
    document.getElementById("btnByPhone")?.addEventListener("click", sortByPhone);
    document.getElementById("btnByAddress")?.addEventListener("click", sortByAddress);
}

const addPerson = () => {
    let name = (document.getElementById("txtName") as HTMLInputElement).value;
    let phone = (document.getElementById("txtPhone") as HTMLInputElement).value;
    let address = (document.getElementById("txtAddress") as HTMLInputElement).value;
    let email = (document.getElementById("txtEmail") as HTMLInputElement).value;
    if (name != "" && phone != "" && address != "" && email != "") {
        people.push({ name: name, phone: phone, address: address, email: email });
    }
    fillTable();
}


const sortByName = () => {
    people.sort((x, y) => x.name.localeCompare(y.name));
    fillTable();
}

const sortByPhone = () => {
    people.sort((x, y) => parseInt(x.phone) - parseInt(y.phone));
    fillTable();
}

const sortByAddress = () => {
    people.sort((x, y) => parseInt(x.address.substring(0, 3)) - parseInt(y.address.substring(0, 3)));
    fillTable();
}

// TODO: Doesn't work.
const sortByEmail = () => {
    people.sort((x, y) => x.email.localeCompare(y.email));
    fillTable();
}

let currentTemp: any;

if(document.getElementById("currentTemp") != null) {
    currentTemp = document.getElementById("currentTemp");
}

const getWeather = async(): Promise<void> => {
    await fetch("https://api.openweathermap.org/data/2.5/weather?q=kansas city&units=imperial&appid=6c9a42ba86a6f92778dd143ee0843282")
    .then((result: Response) => result.json())
    .then((data) => currentTemp.innerText = data.main.temp + "F");
}

document.getElementById("btnAddPerson")?.addEventListener("click", addPerson);
document.getElementById("btnGetWeather")?.addEventListener("click", getWeather);
document.getElementById("btnByName")?.addEventListener("click", sortByName);
document.getElementById("btnByPhone")?.addEventListener("click", sortByPhone);
document.getElementById("btnByAddress")?.addEventListener("click", sortByAddress);
document.getElementById("btnByEmail")?.addEventListener("click", sortByEmail);

// add the table and fill it.
let body = document.getElementById("main")?.appendChild(personTable);
personTable.style.width = "100%";
personTable.style.borderStyle = "solid";
personTable.style.textAlign = "left";
fillTable();
