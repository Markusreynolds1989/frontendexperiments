"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var _a, _b, _c, _d, _e, _f, _g;
let people = [];
let colorSwitch = 0;
let personTable = document.createElement("table");
people.push({ name: "tom", phone: "5555555555", address: "23 bakers", email: "tom@gmail.com" });
const first = (input) => input[0];
const rest = (input) => input.slice(1);
const cons = (list1, list2) => list1.concat(list2);
const createHeader = () => {
    let header = document.createElement("thead");
    let headerRow = document.createElement("tr");
    headerRow.style.backgroundColor = "lightgrey";
    for (let field in people[0]) {
        let title = cons(first(field).toUpperCase(), rest(field));
        // Create the data
        let headerValue = document.createTextNode(title);
        // create the cells for to hold the data
        let tableHeader = document.createElement("th"); // th
        // button
        let button = document.createElement("button");
        button.id = `btnBy${cons(first(field).toUpperCase(), rest(field))}`;
        button.textContent = cons(first(field).toUpperCase(), rest(field));
        button.style.borderStyle = "none";
        button.style.backgroundColor = "lightgrey";
        button.style.fontSize = "16px";
        button.style.fontStyle = "bold";
        button.style.textAlign = "left";
        button.style.paddingLeft = "0px";
        // insert the data into the cell
        //tableHeader.appendChild(headerValue);
        tableHeader.appendChild(button);
        // add the cell to the row
        headerRow.appendChild(tableHeader);
    }
    header.appendChild(headerRow);
    // Add the row to the table
    personTable === null || personTable === void 0 ? void 0 : personTable.append(header);
};
const fillTable = () => {
    var _a, _b, _c, _d;
    colorSwitch = 0;
    personTable.innerHTML = "";
    createHeader();
    let tableBody = document.createElement("tbody");
    for (let person of people) {
        let row = document.createElement("tr");
        if (colorSwitch % 2 == 0) {
            row.style.backgroundColor = "gainsboro";
        }
        colorSwitch += 1;
        // Create the data for the cells.
        let rowName = document.createTextNode(person.name);
        let rowPhone = document.createTextNode(person.phone);
        let rowAddress = document.createTextNode(person.address);
        let rowEmail = document.createTextNode(person.email);
        // Create the cells.
        let tableRowName = document.createElement("td");
        let tableRowPhone = document.createElement("td");
        let tableRowAddress = document.createElement("td");
        let tableRowEmail = document.createElement("td");
        // put the data into the cells.
        tableRowName.appendChild(rowName);
        tableRowPhone.appendChild(rowPhone);
        tableRowAddress.appendChild(rowAddress);
        tableRowEmail.appendChild(rowEmail);
        // add the cells to the row.
        row.appendChild(tableRowName);
        row.appendChild(tableRowPhone);
        row.appendChild(tableRowAddress);
        row.appendChild(tableRowEmail);
        //add row to table
        tableBody.appendChild(row);
    }
    personTable.appendChild(tableBody);
    (_a = document.getElementById("btnAddPerson")) === null || _a === void 0 ? void 0 : _a.addEventListener("click", addPerson);
    (_b = document.getElementById("btnByName")) === null || _b === void 0 ? void 0 : _b.addEventListener("click", sortByName);
    (_c = document.getElementById("btnByPhone")) === null || _c === void 0 ? void 0 : _c.addEventListener("click", sortByPhone);
    (_d = document.getElementById("btnByAddress")) === null || _d === void 0 ? void 0 : _d.addEventListener("click", sortByAddress);
};
const addPerson = () => {
    let name = document.getElementById("txtName").value;
    let phone = document.getElementById("txtPhone").value;
    let address = document.getElementById("txtAddress").value;
    let email = document.getElementById("txtEmail").value;
    if (name != "" && phone != "" && address != "" && email != "") {
        people.push({ name: name, phone: phone, address: address, email: email });
    }
    fillTable();
};
const sortByName = () => {
    people.sort((x, y) => x.name.localeCompare(y.name));
    fillTable();
};
const sortByPhone = () => {
    people.sort((x, y) => parseInt(x.phone) - parseInt(y.phone));
    fillTable();
};
const sortByAddress = () => {
    people.sort((x, y) => parseInt(x.address.substring(0, 3)) - parseInt(y.address.substring(0, 3)));
    fillTable();
};
// TODO: Doesn't work.
const sortByEmail = () => {
    people.sort((x, y) => x.email.localeCompare(y.email));
    fillTable();
};
let currentTemp;
if (document.getElementById("currentTemp") != null) {
    currentTemp = document.getElementById("currentTemp");
}
const getWeather = () => __awaiter(void 0, void 0, void 0, function* () {
    yield fetch("https://api.openweathermap.org/data/2.5/weather?q=kansas city&units=imperial&appid=6c9a42ba86a6f92778dd143ee0843282")
        .then((result) => result.json())
        .then((data) => currentTemp.innerText = data.main.temp + "F");
});
(_a = document.getElementById("btnAddPerson")) === null || _a === void 0 ? void 0 : _a.addEventListener("click", addPerson);
(_b = document.getElementById("btnGetWeather")) === null || _b === void 0 ? void 0 : _b.addEventListener("click", getWeather);
(_c = document.getElementById("btnByName")) === null || _c === void 0 ? void 0 : _c.addEventListener("click", sortByName);
(_d = document.getElementById("btnByPhone")) === null || _d === void 0 ? void 0 : _d.addEventListener("click", sortByPhone);
(_e = document.getElementById("btnByAddress")) === null || _e === void 0 ? void 0 : _e.addEventListener("click", sortByAddress);
(_f = document.getElementById("btnByEmail")) === null || _f === void 0 ? void 0 : _f.addEventListener("click", sortByEmail);
// add the table and fill it.
let body = (_g = document.getElementById("main")) === null || _g === void 0 ? void 0 : _g.appendChild(personTable);
personTable.style.width = "100%";
personTable.style.borderStyle = "solid";
personTable.style.textAlign = "left";
fillTable();
